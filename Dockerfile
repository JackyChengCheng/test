FROM registry.new.dataos.io/mlp-dev-k8s/conda_py3.6_cuda10.0_base:3.1.6.0


USER root

#copy file
COPY docker-entrypoint-drimo.sh /work/ai_lab/miner/script


# set entrypoint
ENTRYPOINT ["/work/ai_lab/miner/script/docker-entrypoint-drimo.sh"]


# install package
RUN  pip install tensorflow_gpu==1.14.0 --timeout 300
 


